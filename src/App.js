import React, { Component } from 'react';
import './App.css';
import Person from './Person/Person';

class App extends Component {
  state = {
    persons: [
      { id:"12CD", name: 'Manu', age: 29 },
      { id:"12AB", name: 'Max', age: 28 },
      { id:"12EF", name: 'Stephanie', age: 26 }
    ],
    otherState: 'some other value',
    showPersons: false
  }

  nameChangedHandler = ( event, id ) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id ===id;
    }); //Get the ID of the person element within the array
    //const person = this.state.persons[personIndex]; //this acts on the original object and it's not the best way
    const person = {
      ...this.state.persons[personIndex]
    }; //this creates a new javascript object. It distributes all the propreties from the fetched object into a new object

    //const person = Object.assign({}, this.state.persons[personIndex]); //It represents the alternative of the previous spread operator

    person.name = event.target.value;
    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState( {
      persons: persons
    } )
  }

  deletePersonHandler = (personIndex) => {
    //const persons = this.state.persons; //This is a bad practice because of we work on the original object
    //const persons = this.state.persons.slice(); //instead use slice that creates a new copy
    const persons = [...this.state.persons]; //or use the ES6 spread operator syntax
    persons.splice(personIndex, 1); //Delete the element at the specified index
    this.setState({persons: persons})
  }

  togglePersonsHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState( { showPersons: !doesShow } );
  }

  render () {
    const style = {
      backgroundColor: 'white',
      font: 'inherit',
      border: '1px solid blue',
      padding: '8px',
      cursor: 'pointer'
    };

    let persons = null;

    if ( this.state.showPersons ) {
      persons = (
        <div>
          {this.state.persons.map((person, index) => {
            return <Person 
            key={person.id}
            click={() => this.deletePersonHandler(index)}
            name={person.name} 
            age={person.age} 
            changed={(event) =>this.nameChangedHandler(event, person.id)}
            />
          })}
          {/* <Person
            name={this.state.persons[0].name}
            age={this.state.persons[0].age} />
          <Person
            name={this.state.persons[1].name}
            age={this.state.persons[1].age}
            click={this.switchNameHandler.bind( this, 'Max!' )}
            changed={this.nameChangedHandler} >My Hobbies: Racing</Person>
          <Person
            name={this.state.persons[2].name}
            age={this.state.persons[2].age} /> */}
        </div>
      );
    }

    return (
      <div className="App">
        <h1>Hi, I'm a React App</h1>
        <p>This is really working!</p>
        <button
          style={style}
          onClick={this.togglePersonsHandler}>Toggle Persons</button>
        {persons}
      </div>
    );
    // return React.createElement('div', {className: 'App'}, React.createElement('h1', null, 'Does this work now?'));
  }
}

export default App;
